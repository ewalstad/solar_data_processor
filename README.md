# Solar Data Parser

A toy project to

1. Parse energy data from an Outback solar charge controller
1. Play with GitLab's ci/cd tools
    * Run the tests
    * Build a docker image
    * Push the image to the GitLab container registry

## Developer Workflow

* Use git locally
* activate the venv with `source venv/bin/activate`
* test locally with `python tests.py`
* run locally with
  `./solar_data_parser.py 2020-12-01 2.0 103.0 2.8 1.0 60.0 1.6`
* build docker locally with
  `docker build -t solar_data_processor:local-build .`
* run the local docker with
  ```
  docker run --rm -it \
  solar_data_processor:local-build \
  2020-12-01 2.0 103.0 2.8 1.0 60.0 1.6
  ```
* fetch the latest production container with
  ```
  docker pull registry.gitlab.com/ewalstad/solar_data_processor
  ```
* run the production container locally with
  ```
  docker run --rm -it \
  registry.gitlab.com/ewalstad/solar_data_processor \
  2020-12-01 2.0 103.0 2.8 1.0 60.0 1.6
  ```

## CI/CD

Builds are triggered when code is pushed to the origin,
[gitlab](https://gitlab.com/ewalstad/solar_data_processor). Each build includes
running the tests. If the tests fail, then the build stage will not be run.
See the [.gitlab-ci.yml](.gitlab-ci.yml) file for details.

### develop

From any branch other than `main`:

* Run tests and build a docker image tagged with the branch name with
  `git push origin`
  * This will create a docker image in gitlab, tagged with eg
    `ewalstad-develop`

### production

From the `main` branch:

* Run tests and build a production-ready docker image with
  `git push origin`


## Example Command Line Usage:

```shell script
docker run --rm -it \
solar_data_processor:local-build \
2020-12-01 2.0 103.0 2.8 1.0 60.0 1.6 
```

