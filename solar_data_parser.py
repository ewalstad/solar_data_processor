#!/usr/bin/env python
"""
Parses a string of data representing daily totals of energy collected.

See the unit tests for examples.
"""
import sys
import datetime
import arrow
import decimal


def parse_transcribed_data_string(data_string):
    """
    Parses the data_string and returns a list of dictionaries containing the
    following keys:

        date: a datetime object representing the day the energy was collected
        Ah: The daily total amp-hours of energy collected
        kWh: Theh daily total kilowatt-hours of energy collected

    data_string must be in the format:
    m/d/yyyy <day offset> <kWh int> <Ah float>[ <day offset> <kWh int> <Ah float>...]
    eg:
    2020-08-22 10 111 3.0 9 129 3.4 8 163 4.4 7 175 4.6
    """
    ret_val = []
    bits = data_string.split()
    d = bits.pop(0)
    start_date = arrow.get(d).datetime.date()

    for idx, bit in enumerate(bits):
        if idx%3 == 0:
            days = abs(int(decimal.Decimal(bits[idx])))
            this_day = start_date - datetime.timedelta(days=days)
            ret_val.append(
                dict(
                    date=this_day,
                    Ah=int(decimal.Decimal(bits[idx + 1])),
                    kWh=float(bits[idx + 2])
                )
            )
        if idx >= len(bits) - 3:
            # We just parsed the last day's data
            break
    return ret_val


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(
            "You must supply a data string to parse. See tests.py for examples"
        )
        sys.exit(1)

    data_string = " ".join(sys.argv[1:])
    for row in parse_transcribed_data_string(data_string):
        print("%s\t%s\t%s" % (row['date'], row['Ah'], row['kWh']))
