FROM python:3.8-slim-buster

# Use a non-root user
RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

# Use a virtualenv
ENV VIRTUAL_ENV=/home/appuser/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies
COPY requirements.txt /tmp/
#RUN pip install --upgrade pip
RUN pip install -r /tmp/requirements.txt

# Run the application
COPY solar_data_parser.py .
ENTRYPOINT ["python", "solar_data_parser.py"]
