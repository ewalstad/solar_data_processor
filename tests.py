import unittest
import datetime
from datetime import date
from freezegun import freeze_time


from solar_data_parser import parse_transcribed_data_string

@freeze_time("2020-09-02")
class SolarDataProcessorTestCase(unittest.TestCase):
    def test_parse_transcribed_data_string(self):
        # d will be the same as our `freeze_time`
        d = datetime.datetime.now().date()
        s = str(d) + " 5 111 3.0 -4 129 3.4 -3 163 4.4 -2 175 4.6 -1 176 4.7"
        expected = [
            {"date": date(2020, 8, 28), "Ah":111, "kWh":3.0},
            {"date": date(2020, 8, 29), "Ah":129, "kWh":3.4},
            {"date": date(2020, 8, 30), "Ah":163, "kWh":4.4},
            {"date": date(2020, 8, 31), "Ah":175, "kWh":4.6},
            {"date": date(2020, 9, 1), "Ah":176, "kWh":4.7},
        ]
        self.assertEqual(
            parse_transcribed_data_string(data_string=s),
            expected
        )

    def test_parse_data_string_with_date(self):
        s = "1999-08-28 10 111 3.0 9 129 3.4 8 163 4.4 7 175 4.6"
        expected = [
            {"date": date(1999, 8, 18), "Ah":111, "kWh":3.0},
            {"date": date(1999, 8, 19), "Ah":129, "kWh":3.4},
            {"date": date(1999, 8, 20), "Ah":163, "kWh":4.4},
            {"date": date(1999, 8, 21), "Ah":175, "kWh":4.6},
        ]
        self.assertEqual(
            parse_transcribed_data_string(data_string=s),
            expected
        )

    def test_parse_decimal_days_and_ah_data(self):
        """It is less error prone to dictate decimal values for day & Ah"""
        d = datetime.datetime.now().date()
        s = str(d) + " 3.0 70.7 1.9 2.0 103.0 2.8 1.0 60.0 1.6"
        expected = [
            {"date": date(2020, 8, 30), "Ah":70, "kWh":1.9},
            {"date": date(2020, 8, 31), "Ah":103, "kWh":2.8},
            {"date": date(2020, 9, 1), "Ah":60, "kWh":1.6},
        ]
        self.assertEqual(
            parse_transcribed_data_string(data_string=s),
            expected
        )

if __name__ == '__main__':
    unittest.main()
